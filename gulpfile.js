'use strict';

var gulp = require('gulp');
var watch = require('gulp-watch');

gulp.task('default', function() {
  watch('**/*', { verbose: true }, function(vinyl) {
    console.log('history: ', vinyl.history, typeof vinyl.history);
    console.log('cwd:', vinyl.cwd, typeof vinyl.cwd);
    console.log('base:', vinyl.base, typeof vinyl.base);
    console.log('stat:', vinyl.stat, typeof vinyl.stat);
    console.log('_contents:', vinyl._contents, typeof vinyl._contents);
    console.log('event:', vinyl.event, typeof vinyl.event);
  });
});
