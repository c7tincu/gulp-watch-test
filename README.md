gulp-watch-test
===============

Build & Run Instructions
------------------------

Tested with `node@0.10.33` & `npm@2.6.1`.

Please make sure you install `npm@2.6.1` by running `sudo npm i -g npm@2.6.1`, right after installing Node.

```
npm i
npm run gulp
```
